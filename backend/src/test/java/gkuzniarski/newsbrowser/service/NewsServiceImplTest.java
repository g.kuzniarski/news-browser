package gkuzniarski.newsbrowser.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import gkuzniarski.newsbrowser.BaseApplicationTests;
import gkuzniarski.newsbrowser.configuration.feign.NewsApiOrgClient;
import gkuzniarski.newsbrowser.controller.NewsController;
import gkuzniarski.newsbrowser.controller.RestResponseEntityExceptionHandler;
import gkuzniarski.newsbrowser.dto.NewsDto;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class NewsServiceImplTest extends BaseApplicationTests {

    @Autowired
    private NewsService newsService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private NewsController newsController;

    @Test
    void whenCountryAndCategoryMixedWithSources_thenIllegalArgumentExceptionThrown() {
        assertThrows(IllegalArgumentException.class, () -> {
            newsService.getNewsPage("A", "B", "C", null, PageRequest.of(0, 1));
        });
    }

    @Test
    void whenArgumentsCorrect_thenResultNotNull() {
        String country = "pl";
        String category = "technology";

        NewsDto expected = NewsDto.builder()
                .country(country)
                .category(category)
                .articles(new PageImpl<>(new ArrayList<>(), PageRequest.of(0, 1), 0))
                .build();

        NewsDto actual = newsService.getNewsPage(country, category, null, null, PageRequest.of(0, 1));

        assertEquals(expected, actual);
    }

    @Test
    void givenMockMvc_whenArgumentsCorrect_thenResultNotNull() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(newsController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();

        String country = "pl";
        String category = "technology";

        String expected = objectMapper.writeValueAsString(NewsDto.builder()
                .country(country)
                .category(category)
                .articles(new PageImpl<>(new ArrayList<>(), PageRequest.of(0, 1), 0))
                .build());

        mockMvc.perform(get(String.format("/news/%s/%s", country, category)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void givenMockMvc_whenCountryTest_thenResultWithArticles() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(newsController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();

        String country = "test";
        String category = "technology";
        List<NewsDto.ArticleDto> articles = Lists.newArrayList(NewsDto.ArticleDto.builder()
                .articleUrl("gkuzniarski.newsbrowser")
                .author("Grzegorz")
                .date("2019-11-13T09:11:11")
                .description("Test in progress")
                .imageUrl("gkuzniarski.newsbrowser/img.jpg")
                .sourceName("Tests")
                .title("Test")
                .build());

        String expected = objectMapper.writeValueAsString(NewsDto.builder()
                .country(country)
                .category(category)
                .articles(new PageImpl<>(articles, PageRequest.of(0, 1), 1))
                .build());

        mockMvc.perform(get(String.format("/news/%s/%s", country, category)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }


    @TestConfiguration
    public static class TestBeanConfiguration {
        @Bean
        @Primary
        NewsApiOrgClient testNewsApiOrgClient() {
            return new NewsApiOrgClient() {
                public NewsApiOrgDto getNews(NewsApiParameters parameters) {
                    NewsApiOrgDto newsApiOrgDto = new NewsApiOrgDto();
                    newsApiOrgDto.setStatus("ok");

                    if ("test".equals(parameters.getCountry())) {
                        newsApiOrgDto.setTotalResults(1);

                        NewsApiOrgDto.ArticleNewsApiOrgDto article = new NewsApiOrgDto.ArticleNewsApiOrgDto();
                        article.setAuthor("Grzegorz");
                        article.setContent("NewsController tests");
                        article.setDescription("Test in progress");
                        article.setPublishedAt("2019-11-13T09:11:11");
                        NewsApiOrgDto.ArticleNewsApiOrgDto.Source source = new NewsApiOrgDto.ArticleNewsApiOrgDto.Source();
                        source.setId("1");
                        source.setName("Tests");
                        article.setSource(source);
                        article.setTitle("Test");
                        article.setUrl("gkuzniarski.newsbrowser");
                        article.setUrlToImage("gkuzniarski.newsbrowser/img.jpg");

                        newsApiOrgDto.getArticles().add(article);
                        return newsApiOrgDto;
                    } else {
                        newsApiOrgDto.setTotalResults(0);
                        return newsApiOrgDto;
                    }
                }
            };
        }
    }
}
