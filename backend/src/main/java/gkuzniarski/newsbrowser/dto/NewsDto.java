package gkuzniarski.newsbrowser.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Page;

import javax.validation.constraints.Pattern;

@Data
@Builder
public class NewsDto {

    private String country;
    private String category;
    private Page<ArticleDto> articles;

    @Data
    @Builder
    public static class ArticleDto {
        private String author;
        private String title;
        private String description;
        @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$")
        private String date;
        private String sourceName;
        private String articleUrl;
        private String imageUrl;
    }
}
