package gkuzniarski.newsbrowser.configuration.feign;

import com.fasterxml.jackson.annotation.JsonInclude;
import feign.QueryMap;
import feign.RequestLine;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

public interface NewsApiOrgClient {
    @RequestLine("GET /top-headlines")
    NewsApiOrgDto getNews(@QueryMap NewsApiParameters queryMap);

    @Data
    class NewsApiOrgDto {
        private String status;
        private long totalResults;
        private List<ArticleNewsApiOrgDto> articles = new ArrayList<>();

        @Data
        public static class ArticleNewsApiOrgDto {
            private Source source = new Source();
            private String author;
            private String title;
            private String description;
            private String url;
            private String urlToImage;
            private String publishedAt;
            private String content;

            @Data
            public static class Source {
                private String id;
                private String name;
            }
        }
    }

    @Data
    @Builder
    class NewsApiParameters {
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private String country;

        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private String sources;

        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private String category;


        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private String q;

        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private Integer pageSize;

        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private Integer page;
    }
}
