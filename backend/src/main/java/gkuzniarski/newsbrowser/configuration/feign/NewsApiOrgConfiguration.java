package gkuzniarski.newsbrowser.configuration.feign;

import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;
import gkuzniarski.newsbrowser.configuration.NewsApiOrgProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class NewsApiOrgConfiguration {

    private final NewsApiOrgProperties properties;

    public NewsApiOrgConfiguration(NewsApiOrgProperties properties) {
        this.properties = properties;
    }

    @Bean
    public NewsApiOrgClient newsApiOrgClient() {
        return Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .requestInterceptor(requestTemplate -> requestTemplate.header("X-Api-Key", properties.getXApiKey()))
                .options(new Request.Options(properties.getConnectTimeoutMillis(), properties.getReadTimeoutMillis()))
                .logger(new Slf4jLogger(NewsApiOrgClient.class))
                .logLevel(Logger.Level.FULL)
                .target(NewsApiOrgClient.class, properties.getUrl());
    }
}
