package gkuzniarski.newsbrowser.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;
import java.time.temporal.ChronoUnit;
import java.util.Collection;

@Data
@Configuration
@ConfigurationProperties(prefix = "news-api-org")
public class NewsApiOrgProperties {

    @NotNull
    private Collection<String> xApiKey;

    @DurationUnit(ChronoUnit.MILLIS)
    private int connectTimeoutMillis;

    @DurationUnit(ChronoUnit.MILLIS)
    private int readTimeoutMillis;

    private String url;
}
