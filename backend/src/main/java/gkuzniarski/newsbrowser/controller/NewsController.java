package gkuzniarski.newsbrowser.controller;

import gkuzniarski.newsbrowser.service.NewsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("news")
@Slf4j
public class NewsController {

    private final NewsService newsService;

    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping("")
    public ResponseEntity getNewsPage(@RequestParam(required = false) String country,
                                      @RequestParam(required = false) String category,
                                      @RequestParam(required = false) String sources,
                                      @RequestParam(required = false) String q,
                                      @RequestParam(required = false) Integer page,
                                      @RequestParam(required = false) Integer pageSize) {
        log.debug(String.format("Request GET 'news' with parameters: %s, %s, %s, %s, %d, %d", country, category, sources, q, page, pageSize));
        return ResponseEntity.ok(newsService.getNewsPage(country, category, sources, q, PageRequest.of(page, pageSize)));
    }

    @GetMapping("/{country}/{category}")
    public ResponseEntity getNewsByCountryAndCategory(@PathVariable String country,
                                                      @PathVariable String category,
                                                      @RequestParam(required = false) String q,
                                                      @RequestParam(required = false, defaultValue = "0") Integer page,
                                                      @RequestParam(required = false, defaultValue = "1") Integer pageSize) {
        log.debug(String.format("Request GET 'news/%s/%s' with parameters: %s, %d, %d", country, category, q, page, pageSize));
        return ResponseEntity.ok(newsService.getNewsPage(country, category, null, q, PageRequest.of(page, pageSize)));
    }
}
