package gkuzniarski.newsbrowser.service;

import gkuzniarski.newsbrowser.configuration.feign.NewsApiOrgClient;
import gkuzniarski.newsbrowser.dto.NewsDto;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NewsServiceImpl implements NewsService {

    private final NewsApiOrgClient client;

    public NewsServiceImpl(NewsApiOrgClient client) {
        this.client = client;
    }

    @Override
    public NewsDto getNewsPage(String country, String category, String sources, String q, Pageable pageable) {
        if (StringUtils.isEmpty(sources) && StringUtils.isEmpty(country) && StringUtils.isEmpty(category)) {
            throw new IllegalArgumentException("One of parameters must be given: country, category, sources");
        }
        if (!StringUtils.isEmpty(sources) && (!StringUtils.isEmpty(country) || !StringUtils.isEmpty(category))) {
            throw new IllegalArgumentException("Sources parameter cannot be mixed with country or category");
        }

        NewsApiOrgClient.NewsApiOrgDto news = client.getNews(
                NewsApiOrgClient.NewsApiParameters.builder()
                        .country(country)
                        .category(category)
                        .sources(sources)
                        .q(q)
                        .pageSize(pageable.getPageSize())
                        .page(pageable.getPageNumber() + 1)
                        .build()
        );

        List<NewsDto.ArticleDto> articleDtos = new ArrayList<>();

        if (news.getArticles() != null) {
            articleDtos = news.getArticles()
                    .stream()
                    .map(this::mapToNewsDto)
                    .collect(Collectors.toList());
        }

        return NewsDto.builder()
                .articles(new PageImpl<>(
                        articleDtos,
                        pageable,
                        news.getTotalResults()))
                .category(category)
                .country(country)
                .build();
    }

    private NewsDto.ArticleDto mapToNewsDto(NewsApiOrgClient.NewsApiOrgDto.ArticleNewsApiOrgDto dto) {
        return NewsDto.ArticleDto.builder()
                .author(dto.getAuthor())
                .title(dto.getTitle())
                .description(dto.getDescription())
                .date(dto.getPublishedAt())
                .sourceName(dto.getSource().getName())
                .articleUrl(dto.getUrl())
                .imageUrl(dto.getUrlToImage())
                .build();
    }
}
