package gkuzniarski.newsbrowser.service;

import gkuzniarski.newsbrowser.dto.NewsDto;
import org.springframework.data.domain.Pageable;

public interface NewsService {
    NewsDto getNewsPage(String country, String category, String sources, String q, Pageable pageable);
}
