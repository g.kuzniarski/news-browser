import {Page} from './page';

export class News {
  country?: string;
  category?: string;
  articles: Page<Article>;
}

export class Article {
  author: string;
  title: string;
  description: string;
  date: string;
  sourceName: string;
  articleUrl: string;
  imageUrl: string;
}
