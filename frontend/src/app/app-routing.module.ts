import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NewsComponent} from './component/news/news.component';
import {AboutComponent} from './component/about/about.component';

export const ROUTES: Routes = [
  {path: '', redirectTo: '/news', pathMatch: 'full', data: {hidden: true}},
  {path: 'news', component: NewsComponent, data: {name: 'News'}},
  {path: 'about', component: AboutComponent, data: {name: 'About me'}}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
