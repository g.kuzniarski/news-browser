import {Component, EventEmitter, OnInit} from '@angular/core';
import {NewsService} from '../../service/news/news.service';
import {Article} from '../../model/news';
import {Observable} from 'rxjs';
import {PageEvent} from '@angular/material';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  private articleList$: Observable<Article[]>;
  private articleEmitter = new EventEmitter<Article[]>();

  isArticleLoading = false;
  isServiceUnavailable = false;

  length = 0;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions: number[] = [1, 3, 5, 10];

  newsForm = {
    country: 'pl',
    category: 'technology',
    sources: '',
    q: ''
  };

  languages = [
    {value: 'pl', viewValue: 'polski'},
    {value: 'ae', viewValue: 'ae'},
    {value: 'ar', viewValue: 'ar'},
    {value: 'at', viewValue: 'at'},
    {value: 'au', viewValue: 'au'},
    {value: 'be', viewValue: 'be'},
    {value: 'bg', viewValue: 'bg'},
    {value: 'br', viewValue: 'br'},
    {value: 'ca', viewValue: 'ca'},
    {value: 'ch', viewValue: 'ch'},
    {value: 'cn', viewValue: 'cn'},
    {value: 'co', viewValue: 'co'},
    {value: 'cu', viewValue: 'cu'},
    {value: 'cz', viewValue: 'cz'},
    {value: 'de', viewValue: 'de'},
    {value: 'eg', viewValue: 'eg'},
    {value: 'fr', viewValue: 'fr'},
    {value: 'gb', viewValue: 'gb'},
    {value: 'gr', viewValue: 'gr'},
    {value: 'hk', viewValue: 'hk'},
    {value: 'hu', viewValue: 'hu'},
    {value: 'id', viewValue: 'id'},
    {value: 'ie', viewValue: 'ie'},
    {value: 'il', viewValue: 'il'},
    {value: 'in', viewValue: 'in'},
    {value: 'it', viewValue: 'it'},
    {value: 'jp', viewValue: 'jp'},
    {value: 'kr', viewValue: 'kr'},
    {value: 'lt', viewValue: 'lt'},
    {value: 'lv', viewValue: 'lv'},
    {value: 'ma', viewValue: 'ma'},
    {value: 'mx', viewValue: 'mx'},
    {value: 'my', viewValue: 'my'},
    {value: 'ng', viewValue: 'ng'},
    {value: 'nl', viewValue: 'nl'},
    {value: 'no', viewValue: 'no'},
    {value: 'nz', viewValue: 'nz'},
    {value: 'ph', viewValue: 'ph'},
    {value: 'pt', viewValue: 'pt'},
    {value: 'ro', viewValue: 'ro'},
    {value: 'rs', viewValue: 'rs'},
    {value: 'ru', viewValue: 'ru'},
    {value: 'sa', viewValue: 'sa'},
    {value: 'se', viewValue: 'se'},
    {value: 'sg', viewValue: 'sg'},
    {value: 'si', viewValue: 'si'},
    {value: 'sk', viewValue: 'sk'},
    {value: 'th', viewValue: 'th'},
    {value: 'tr', viewValue: 'tr'},
    {value: 'tw', viewValue: 'tw'},
    {value: 'ua', viewValue: 'ua'},
    {value: 'us', viewValue: 'us'},
    {value: 've', viewValue: 've'},
    {value: 'za', viewValue: 'za'}
  ];

  categories = [
    {value: 'business', viewValue: 'Biznes'},
    {value: 'entertainment', viewValue: 'Rozrywka'},
    {value: 'general', viewValue: 'Ogólne'},
    {value: 'health', viewValue: 'Zdrowie'},
    {value: 'science', viewValue: 'Nauka'},
    {value: 'sports', viewValue: 'Sport'},
    {value: 'technology', viewValue: 'Technologia'}
  ];

  constructor(private newsService: NewsService) {
    this.articleList$ = this.articleEmitter;
  }

  ngOnInit() {
    this.getNews(this.pageSize, 0);
  }

  private getNews(pageSize: number, pageIndex: number) {
    this.isArticleLoading = true;
    console.log(this.newsForm.country, this.newsForm.category, this.newsForm.sources, this.newsForm.q, pageIndex, pageSize);
    this.newsService.getNews(this.newsForm.country, this.newsForm.category, this.newsForm.sources, this.newsForm.q, pageIndex, pageSize)
      .subscribe(news => {
        this.articleEmitter.emit(news.articles.content);
        this.length = news.articles.totalElements;
        this.isArticleLoading = false;
        this.isServiceUnavailable = false;
      }, () => {
        this.articleEmitter.emit([]);
        this.isArticleLoading = false;
        this.isServiceUnavailable = true;
      });
  }

  changePage($event: PageEvent) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    this.getNews(this.pageSize, this.pageIndex);
  }

  refresh() {
    this.newsForm = {
      country: 'pl',
      category: 'technology',
      sources: '',
      q: ''
    };
    this.getNews(this.pageSize, 0);
  }
}
