import {Component, OnInit} from '@angular/core';
import {Router, Routes} from '@angular/router';
import {ROUTES} from '../../app-routing.module';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private routes: Routes;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.routes = ROUTES.filter(value => !value.data.hidden);
  }

  navigateTo(route: string) {
    this.router.navigate([route]);
  }
}
