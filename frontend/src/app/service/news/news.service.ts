import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {News} from '../../model/news';
import {BaseHttpServiceService, HttpParamsBuilder} from '../base-http-service.service';

@Injectable({
  providedIn: 'root'
})
export class NewsService extends BaseHttpServiceService {

  constructor(private http: HttpClient) {
    super();
  }

  /** GET news from the server */
  getNewsByCounryAndCategory(country: string, category: string, q?: string, page?: number, pageSize?: number): Observable<News> {
    const newsUrl = `${this.API_BASE_URL}/news/${country}/${category}/`;
    const params = HttpParamsBuilder.builder()
      .append('q', q)
      .append('page', String(page))
      .append('pageSize', String(pageSize))
      .build();

    return this.http.get<News>(newsUrl, {params});
  }

  /** GET news from the server */
  getNews(country?: string, category?: string, sources?: string, q?: string, page?: number, pageSize?: number): Observable<News> {
    const newsUrl = `${this.API_BASE_URL}/news`;
    const params = HttpParamsBuilder.builder()
      .append('country', country)
      .append('category', category)
      .append('sources', sources)
      .append('q', q)
      .append('page', String(page))
      .append('pageSize', String(pageSize))
      .build();

    return this.http.get<News>(newsUrl, {params});
  }
}
