import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BaseHttpServiceService {

  protected API_BASE_URL = 'http://localhost:8080/api';

  constructor() {
  }

  protected appendToHttpParams(params: HttpParams, key: string, value: string): HttpParams {
    if (value !== undefined && key !== undefined) {
      params.append(key, value);
    }
    return params;
  }
}

export class HttpParamsBuilder {
  private static httpParamsBuilder: HttpParamsBuilder;
  private params: HttpParams;

  private constructor() {
  }

  public static builder(): HttpParamsBuilder {
    this.httpParamsBuilder = new HttpParamsBuilder();
    return this.httpParamsBuilder;
  }

  public append(key: string, value: string): HttpParamsBuilder {
    if (this.params === undefined) {
      this.params = new HttpParams();
    }

    if (value !== undefined && key !== undefined) {
      this.params = this.params.append(key, value);
    }

    return this;
  }

  public build(): HttpParams {
    return this.params;
  }
}
